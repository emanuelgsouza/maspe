import pouchdb from './pouchdb'

export default function (dados) {
  return pouchdb.put(dados)
    .then(function (response) {
      return response
    })
    .catch(function (err) {
      return err
    })
}
