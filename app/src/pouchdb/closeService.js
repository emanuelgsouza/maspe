import pouchdb from './pouchdb'

export default function (id, entrega, motivo, concertado) {
  return pouchdb.get(id)
  .then(function (doc) {
    return pouchdb.put({
      _id: id,
      _rev: doc._rev,
      Nome: doc.Nome,
      Endereco: doc.Endereco,
      Celular: doc.Celular,
      CPF: doc.CPF,
      RG: doc.RG,
      Entrada: doc.Entrada,
      Previsao: doc.Previsao,
      Orcamento: doc.Orcamento,
      Telefone: doc.Telefone,
      Defeito: doc.Defeito,
      Adicionais: doc.Adicionais,
      Entrega: entrega,
      Concertado: concertado,
      Motivo: motivo,
      State: 'close'
    }).then(function (data) {
      return data
    }).catch(function (err) {
      return err
    })
  })
}
