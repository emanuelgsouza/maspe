import PouchDB from 'pouchdb-browser'
import pouchdbFind from 'pouchdb-find'
import replicationStream from 'pouchdb-replication-stream'

PouchDB.plugin(pouchdbFind)
PouchDB.plugin(replicationStream.plugin)
PouchDB.adapter('writableStream', replicationStream.adapters.writableStream)

const maspe = new PouchDB('MASPE')

export default maspe
