import pouchdb from './pouchdb'

export default function () {
  return pouchdb.allDocs()
  .then(function (response) {
    return response.rows
  })
  .catch(function (err) {
    return err
  })
}
