import pouchdb from './pouchdb'

export default function (value, filter) {
  if (filter === 'cliente') {
    return pouchdb.createIndex({
      index: { fields: ['Nome'] }
    })
    .then(function () {
      return pouchdb.find({
        selector: {
          Nome: { $regex: value }
        }
      }).then(function (data) {
        return data.docs
      })
      .catch(function (err) {
        return err
      })
    })
  } else {
    return pouchdb.get(value)
      .then(function (data) {
        return data
      })
      .catch(function (err) {
        return err
      })
  }
}
