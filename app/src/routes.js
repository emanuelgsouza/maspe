export default [
  {
    path: '/',
    name: 'Home',
    component: require('components/Home')
  },
  {
    path: '/add',
    name: 'Add',
    component: require('components/Add')
  },
  {
    path: '/close-service',
    name: 'Close-Service',
    component: require('components/Close')
  },
  {
    path: '/backup-service',
    name: 'Backup-Service',
    component: require('components/Backup')
  }
]
